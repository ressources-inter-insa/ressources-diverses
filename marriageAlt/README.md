# How to generate the graph

The graph respects the [DOT](https://www.graphviz.org/documentation/) file format.

You will need to have a Lapignon emoji file in the same folder of the graph for generation with the name `lapi.png` if you want to have the Lapignon inside the graph.

To generate the graph, you have two way to do so:
1. Use the dot command line: `dot -Tpng graph.dot -o graph.png`
1. Use an online [tool](https://dreampuf.github.io/GraphvizOnline/) that support GraphViz and copy/past the content of the dot file on it to generate the graph

> PS: Using the online tool will forbid you to have a graph with the Lapignon emoji or any other image file ^^
